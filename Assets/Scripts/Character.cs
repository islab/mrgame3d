﻿using UnityEngine;
using UnityEngine.AI;

public class Character : MonoBehaviour
{
    protected NavMeshAgent _navigator;
    protected Animator _animator;
    public float Speed = 3;

    private void Start()
    {
        _navigator = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
    }

    protected virtual void Update()
    {
        _animator.SetFloat("Speed", _navigator.velocity.magnitude);
    }

    public void Attack(Character character)
    {
        _animator.SetTrigger("Attack");
        character.Damage();
    }

    internal void Damage()
    {
        _animator.SetTrigger("Damage");
        enabled = false;
        _navigator.enabled = false;
        StopAllCoroutines();
        Events.RiseOnEnemyKilled(this);
    }
}
