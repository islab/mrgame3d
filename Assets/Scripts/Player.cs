﻿using UnityEngine;

public class Player : Character
{
    #region Params
    private const int RunSpeed = 6;
    public static Player I;
    private float _lastClick;
    public float AttackDistance = 12;
    public Transform FireOut;
    public Bullet BulletPrefab;
    public float Reload = 1;
    private float _reload = 0;
    #endregion

    #region Init
    private void Awake()
    {
        I = this;
    }
    #endregion

    #region Move
    public void Move(Vector3 to)
    {
        _navigator.SetDestination(transform.position + to);
        _navigator.speed = RunSpeed;
    }

    protected override void Update()
    {
        base.Update();
        _reload -= Time.deltaTime;
        var t = CameraController.I.transform;
        if (Input.GetKey(KeyCode.Space))
            Shoot();
        else if (Input.GetKey(KeyCode.W))
            Move(t.forward);
        else if (Input.GetKey(KeyCode.A))
            Move(-t.right);
        else if (Input.GetKey(KeyCode.D))
            Move(t.right);
        else if (Input.GetKey(KeyCode.S))
            Move(-t.forward);
        //else if (Input.GetMouseButtonDown(0))
        //    Move();
    }

    public void Move()
    {
        Speed = _lastClick < Time.time - 0.5f ? 3 : RunSpeed;
        _lastClick = Time.time;
        var ray = CameraController.I.Camera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out var hit))
        {
            _navigator.SetDestination(hit.point);
        }
        _navigator.speed = Speed;
    }
    #endregion

    public void Shoot()
    {
        if (_reload > 0) return;
        _reload = Reload;
        Instantiate(BulletPrefab, FireOut.position, FireOut.rotation);
    }
}
