﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : Character
{
    public float Vision = 15;
    public static readonly List<NPC> Npcs = new List<NPC>();

    private void Awake()
    {
        Npcs.Add(this);
    }

    private void OnDestroy()
    {
        Npcs.Remove(this);
    }

    private void OnEnable()
    {
        StartCoroutine(Behaviour());
    }

    private IEnumerator Behaviour()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            var pos = Player.I.transform.position;
            var distance = (transform.position - pos).magnitude;
            if (Vision < distance) continue;
            _navigator.SetDestination(pos);
            if (distance < 2)
            {
                Attack(Player.I);
            }
            if (!Player.I.enabled)
            {
                _navigator.enabled = false;
                yield break;
            }
        }
    }

    //protected override void Update()
    //{
    //    base.Update();
    //    var pos = Player.I.transform.position;
    //    if (Vision < (transform.position - pos).magnitude)
    //        return;
    //    _navigator.SetDestination(pos);
    //}
}
