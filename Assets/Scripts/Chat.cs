﻿using System;
using UnityEngine;
using UnityEngine.UI;

public static class Events
{
    public static event Action<Character> OnEnemyKilled;

    public static void RiseOnEnemyKilled(Character character) =>
        OnEnemyKilled?.Invoke(character);
}

public class Chat : MonoBehaviour
{
    public static Chat I;
    public Text Text;
    public Text CountText;
    public Button Scrin;
    private int Count;

    private void Awake()
    {
        I = this;
        Text.gameObject.SetActive(false);
        Scrin.onClick.AddListener(() => Player.I.Move());
        Events.OnEnemyKilled += character =>
        {
            if (character == Player.I)
                ThisIsTheEnd();
            else CountText.text = (++Count).ToString();
        };
    }

    public void ThisIsTheEnd()
    {
        Text.text = "GAME\nOVER<size=70>\n\nResult: </size>" + Count;
        Text.gameObject.SetActive(true);
    }

    public void LoadChat()
    {
        Debug.LogError("---");
        //HttpClient
    }
}
