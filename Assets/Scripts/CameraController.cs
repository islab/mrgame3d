﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController I;
    public Camera Camera;
    private Vector3 _prevPos;

    private void Awake()
    {
        I = this;
        Camera = GetComponentInChildren<Camera>();
    }

    private void Update()
    {
        transform.position = Player.I.transform.position;
        if (Input.GetMouseButton(1))
        {
            var delta = _prevPos.x - Input.mousePosition.x;
            transform.Rotate(new Vector3(0, delta, 0));
        }
        _prevPos = Input.mousePosition;
    }
}
