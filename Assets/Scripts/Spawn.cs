﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class Spawn : MonoBehaviour
{
    public float Interval = 3;
    public Character Prefab;

    private void Start()
    {
        StartCoroutine(DoSpawn());
    }

    private IEnumerator DoSpawn()
    {
        while (true)
        {
            yield return new WaitForSeconds(Interval);
            if (NPC.Npcs.Count < 30)
                Instantiate(Prefab, new Vector3(Random.Range(-100, 100),
                    0, Random.Range(-100, 100)), Quaternion.identity);
        }
    }
}
