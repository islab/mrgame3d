﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Rigidbody _rigidbody;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.AddForce(transform.forward * 100, ForceMode.Impulse);
    }

    private void OnTriggerEnter(Collider other)
    {
        var npc = other.GetComponent<NPC>();
        if(other.GetComponent<Player>()) return;
        Destroy(gameObject);
        if(!npc) return;
        npc.Damage();
    }
}